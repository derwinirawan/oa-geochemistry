# OA geochemistry

Proyek ini mengarsipkan penulisan artikel tentang akses terbuka di bidang geokimia di [The Conversation Indonesia](https://theconversation.com/id). Penulis: Dasapta Erwin Irawan ([ORCID](https://orcid.org/0000-0002-1526-0863)) dan Bambang Priadi ([Google Scholar](https://scholar.google.com/citations?hl=en&user=v15Nn54AAAAJ&view_op=list_works&sortby=pubdate)).

Bahan utama dari artikel ini adalah tiga makalah ilmiah yang merupakan kolaborasi internasional peneliti bidang geokimia: 

1. Pourret dkk (2020), [The growth of open access publishing in geochemistry](https://www.sciencedirect.com/science/article/pii/S2666277920300010)
2. Pourret dkk (2020), [Open Access publishing practice in geochemistry: overview of current state and look to the future](https://www.sciencedirect.com/science/article/pii/S2405844020303960)
3. Pourret dkk (2020), [International disparities in open access practices of the Earth Sciences community](https://eartharxiv.org/cpxmy/)

Anda dipersilahkan membaca [manuskripnya](https://gitlab.com/derwinirawan/oa-geochemistry/-/blob/master/TC-OA-GEOCHEMISTRY.md).

Data pendukung: [River Datawrapper](https://river.datawrapper.de/_/igaTv)


