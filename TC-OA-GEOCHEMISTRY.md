# GEOKIMIA TERBUKA

`naskah ini ditulis untuk The Conversation`

Penulis: Dasapta Erwin Irawan ([ORCID](https://orcid.org/0000-0002-1526-0863)) dan Bambang Priadi ([Google Scholar](https://scholar.google.com/citations?hl=en&user=v15Nn54AAAAJ&view_op=list_works&sortby=pubdate))

Bahan: Makalah ilmiah yang ditulis oleh penulis bersama peneliti geokimia lainnya

1. Pourret dkk (2020), [The growth of open access publishing in geochemistry](https://www.sciencedirect.com/science/article/pii/S2666277920300010)
2. Pourret dkk (2020), [Open Access publishing practice in geochemistry: overview of current state and look to the future](https://www.sciencedirect.com/science/article/pii/S2405844020303960)
3. Pourret dkk (2020), [International disparities in open access practices of the Earth Sciences community](https://eartharxiv.org/cpxmy/)



`Pendahuluan`

Geokimia adalah salah satu cabang ilmu geologi yang mempelajari prinsip-prinsip kimia untuk [menjelaskan fenomena geologi](https://books.google.co.uk/books?id=doVGzreGq14C&printsec=frontcover&dq=introduction+to+geochemistry&hl=en&sa=X&ei=HN5fT7juGsmu8AOl05mfBw&ved=0CEIQ6AEwAA#v=onepage&q&f=false). Saat ini obyek ilmu geokimia tidak hanya bebatuan, tetapi juga komponen ilmu bumi lainnya, yaitu hidrosfer (lingkungan air) dan atmosfer (lingkungan udara). Dalam kaitannya dengan bebatuan, ilmu geokimia mengukur komposisi kimia bebatuan untuk menjelaskan proses-proses geologi berskala besar, seperti tektonik (dinamika lempeng bumi) dan vulkanisme (proses kegunungapian), sampai proses mikro seperti pencemaran air tanah (berevolusi menjadi ilmu hidrokimia). 

Ilmu geokimia seperti jauh dari kehidupan sehari-hari, padahal ia dekat sekali. Tidak ada satupun material logam yang anda gunakan yang tidak berasal dari implementasi ilmu geokimia, terutama pada tahap eksplorasi atau pencariannya. Ilmu geokimia juga digunakan saat seorang geologiwan mengeksplorasi cadangan migas atau panas bumi. Sebagai tambahan, geokimia juga salah satu ilmu penting dalam [ranah forensik atau penyelidikan](https://www.lens.org/lens/scholar/article/018-958-193-649-665/main). Dengan pentingnya ilmu geokimia, artikel ini menjelaskan perkembangan ilmu geokimia di Indonesia dan mengapa akses terbuka penting dalam perkembangannya.

`Perkembangan ilmu geokimia di Indonesia dan di dunia`

Ilmu geokimia di Indonesia mulai berkembang pada awal tahun 2000an yang dicerminkan dengan bertambahnya jumlah dokumen ilmiah (jenis dokumen makalah jurnal ilmiah dan non makalah) dengan kata kunci `geokimia` (hanya pada judul) dalam Bahasa Indonesia ([Basis data Lens](https://www.lens.org/lens/search/scholar/list?q=geokimia&preview=true) pada gambar atas). Menurut [basis data Dimensions](https://app.dimensions.ai/discover/publication?search_text=geokimia&search_type=kws&search_field=full_search) (pada gambar bawah) yang khusus mengindeks jurnal ilmiah, kita dapat melihat peningkatan jumlah dokumen pada tahun 2014. 

![Gambar_1](/Users/erwin/Downloads/0-DRAFTS/TC-OA-GEOCHEMISTRY/Gambar_1.png)

Gambar 1 Hasil pencarian dokumen tentang geokimia (dalam Bahasa Indonesia) menggunakan Lens.org

![Gambar_2](/Users/erwin/Downloads/0-DRAFTS/TC-OA-GEOCHEMISTRY/Gambar_2.png)

Gambar 2 Hasil pencarian dokumen tentang geokimia (dalam Bahasa Indonesia) menggunakan Dimensions.ai

Bila ditelaah hingga ke isinya, publikasi ilmiah di bidang geokimia di Indonesia tidak berbeda dengan tema riset di luar negeri, yaitu penggunaan geokimia untuk: geotermal, eksplorasi dan pertambangan, geokimia organik (hidrokarbon), berkaitan dengan beberapa sub bidang ilmu, lingkungan tektonik, serta unsur jejak dan unsur tanah jarang. Hal ini tentunya memberikan keyakinan kepada para peneliti geokimia, bahwa riset di Indonesia tidak tertinggal dari perkembangan riset internasional.



![Gambar_3](/Users/erwin/Downloads/0-DRAFTS/TC-OA-GEOCHEMISTRY/Gambar_3.png)

Gambar 3 Open Knowledge Maps (2020). Overview of research on geokimia. Retrieved from https://openknowledgemaps.org/map/8f1a2f66fb784310f2bac8c8f0290282 [10 Jul 2020].

​    

![Gambar_4](/Users/erwin/Downloads/0-DRAFTS/TC-OA-GEOCHEMISTRY/Gambar_4.png)Gambar 4 Open Knowledge Maps (2020). Overview of research on geochemistry. Retrieved from https://openknowledgemaps.org/map/45151c8f916da091e2f38d5c1700ea1a [10 Jul 2020].    



`Perkembangan akses terbuka`

Akses terbuka atau Open Access  (OA) adalah sebuah gerakan yang dimulai sejak awal 1990an, yang salah satunya ditandai dengan berdirinya [server preprint ArXiv](arxiv.org), tapi [baru berkembang pesat pada tahun 2000an](http://oad.simmons.edu/oadwiki/Timeline). Salah satu indikasinya adalah ditandai dengan [tumbuhnya repositori (gudang dokumen) institusional](https://sustainingknowledgecommons.org/2020/01/03/dramatic-growth-of-open-access-2019/), bertambahnya jumlah [jurnal akses terbuka (OA journal)](https://www.digital-science.com/blog/news/the-ascent-of-open-access-report/). Gerakan ini diinisasi oleh individu dan lembaga yang mengidamkan keterbukaan akses terhadap ilmu pengetahuan yang pada era sebelumnya didominasi oleh sistem penerbitan berlangganan (*subscription-based publishing*). 

Pun di bidang geokimia, kami melihat ada kecenderungan yang sama. Dari 56 jurnal yang dievaluasi oleh [Pourret dkk (2020)](https://www.sciencedirect.com/science/article/pii/S2666277920300010), ada 16 buah jurnal full OA (terbit hanya dalam modus OA), 35 jurnal hybrid (terbit dalam modus OA dan non OA sesuai pilihan penulis), dan 5 jurnal non OA. 



![Gambar_5](/Users/erwin/Downloads/0-DRAFTS/TC-OA-GEOCHEMISTRY/Gambar_5.jpg)

Gambar 5 Perkembangan jurnal dan dokumen OA tentang geokimia di dunia (2000-2020)



Modus hybrid sendiri umumnya digunakan oleh penerbit komersial dengan motif ingin meningkatkan pendapatan dengan cara memberikan [opsi penerbitan non OA atau OA kepada penulis ketika artikelnya diterima (*accepted*)](https://rinarxiv.pubpub.org/pub/6idoxl1e/release/9).  Modus jurnal sendiri dapat saja berubah-ubah menyesuaikan dengan regulasi yang sedang berlaku, sebagai contoh [Plan-S yang diterbitkan oleh Coalition-S di dataran Uni Eropa](https://www.coalition-s.org/). Regulasi tersebut meminta luaran riset yang didanai oleh Coalition-S harus diterbitkan segera di penerbitan daring berjenis OA penuh ([Gold OA atau Diamond OA](https://rinarxiv.pubpub.org/pub/6idoxl1e/release/9)).

> Principle 08: The Funders do not support the ‘hybrid’ model of publishing. However, as a transitional pathway towards full Open Access within a clearly  defined timeframe, and only as part of transformative arrangements,  Funders may contribute to financially supporting such arrangements; [Principles of Plan-S](https://www.coalition-s.org/addendum-to-the-coalition-s-guidance-on-the-implementation-of-plan-s/principles-and-implementation/)

Akibat Plan-S, penerbit komersial yang awalnya menerbitkan jurnal secara hybrid, [mulai menciptakan jurnal kembaran baru](https://thepublicationplan.com/2018/12/06/are-mirror-journals-the-solution-to-publishing-open-access-under-plans/) dalam [modus OA penuh](https://www.elsevier.com/connect/what-are-mirror-journals-and-can-they-offer-a-new-world-of-open-access). Yang menarik di bidang geokimia, [jumlah artikel OA berkorelasi positif dengan Journal Impact Factor (JIF) pada jurnal hybrid, tetapi berkorelasi dengan jurnal OA penuh](https://www.sciencedirect.com/science/article/pii/S2666277920300010).

Akibat dari adanya permintaan (persepsi bahwa [kualitas jurnal ditentukan oleh JIF](https://onlinelibrary.wiley.com/doi/pdf/10.1111/opo.12107)), maka penerbit dapat meningkatkan harga APC (*article processing charge* atau biaya pemrosesan makalah yang dibebankan kepada penulis) secara kontinyu. Walaupun tidak sekuat di bidang lain, terlihat [ada hubungan antara JIF dan APC](https://www.sciencedirect.com/science/article/pii/S2405844020303960) di jurnal-jurnal bidang ilmu geokimia (Gambar 6).  Bila data yang sama dipisahkan berdasarkan jenis penerbitnya (komersial, asosiasi dan penerbit universitas) dapat dilihat bahwa [penerbit komersial mendominasi pasar](https://osf.io/65hzc/?pid=x4gtb). Karena pasar telah berkembang, maka tidak dipungkiri telah terjadi [disparitas perkembangan ilmu](https://eartharxiv.org/cpxmy/) berdasarkan jumlah makalah yang diterbitkan oleh penulis dari negara maju (Kelompok Global North) dan penulis dari negara berkembang atau tertinggal (Kelompok Global South).

 

![https://ars.els-cdn.com/content/image/1-s2.0-S2405844020303960-gr2_lrg.jpg](/Users/erwin/Downloads/0-DRAFTS/TC-OA-GEOCHEMISTRY/Gambar_6.png)

Gambar 6 Hubungan antara JIF dan APC pada bidang ilmu geokimia di dunia pada tahun 2018



`Mengapa OA penting dalam pengembangan ilmu geokimia?`

Demikan ulasan kondisi penerbitan OA yang ternyata sudah pula dikuasai oleh korporasi. Lantas sedemikian pentingkah OA dalam pengembangan ilmu geokimia? Jawabnya adalah ya.

Bila kita perhatikan dana riset bidang geokimia di dunia, menurut [Pangkalan Data Lens](https://www.lens.org/lens/search/scholar/list?q=title:geochemistry&p=0&n=10&s=_score&d=%2B&f=false&e=false&l=en&authorField=author&dateFilterField=publishedYear&presentation=false&preview=&stemmed=true&useAuthorId=false), 10 teratas berasal dari lembaga negara (mayoritas dari Cina). Sayangnya data yang sama tidak muncul untuk riset bidang geokimia di Indonesia, yang mungkin terjadi karena pengkategorian ini tidak pernah dilakukan oleh jurnal-jurnal di dalam negeri.

Tabel 1 Top 10 pemberi dana riset bidang geokimia (menurut Pangkalan Data Lens) 

![Gambar_7](/Users/dasaptaerwinirawan/Downloads/0-DRAFTS/TC-OA-GEOCHEMISTRY/Gambar_7.png)

Dana dari lembaga negara tersebut digunakan untuk membiayai riset bidang geokimia yang cukup mahal, khususnya untuk biaya perjalanan pengembilan data di lapangan dan pengujian laboratorium. Data yang dihasilkan, khususnya untuk bidang studi yang masih sangat sedikit penelitiannya (contoh: unsur tanah jarang) akan sangat bermanfaat bagi peneliti geokimia berikutnya, bilamana dibagikan secara terbuka (OA) setidaknya dengan dua cara, yaitu melalui: jurnal OA (jenis Gold OA atau Diamond OA) dan repositori terbuka (rute [Green OA](http://dasaptaerwin.net/wp/2017/08/how-to-make-your-research-oa-for-free-and-legally.html)). Membuka data secara terbuka (menggunakan [prinsip FAIR data](https://riojournal.com/article/28163/)) juga dapat mengurangi duplikasi riset, dengan demikian riset akan terus bergerak maju ke depan dan melebar ke samping. Riset-riset unsur tanah jarang, khususnya di Indonesia, masih perlu dipacu dengan pesatnya teknologi rekayasa material berkaitan teknologi nano, dalam aplikasinya antara lain untuk teknologi kesehatan dan telekomunikasi.

Demikian uraian kami tentang ilmu geokimia, status risetnya serta upaya keterbukaan data yang perlu dilakukan untuk pengembangannya. Semoga bermanfaat. 

